import React from 'react';
import { StyleSheet, Text, View, SafeAreaView, TextInput, Platform, StatusBar, ScrollView, Image, Dimensions } from 'react-native';
import Category from '../Explore/Category'
import Home from '../Explore/Home'

import Icon from 'react-native-vector-icons/Ionicons'

const {height,width} = Dimensions.get('window')
 export default class Explore extends React.Component {
  componentWillMount(){
    this.startHeaderHeight = 80
    if(Platform.OS == 'android') {
      this.startHeaderHeight = 100 + StatusBar.currentHeight
    }
  }

     render() {
         return (
         <SafeAreaView style={{flex:1}}>
              <View style={{flex:1}}>
                <View style={{height: this.startHeaderHeight, backgroundColor:'white', borderBottomWidth:1, borderBottomColor:'#dddddd'}}>
                  <View style={{flexDirection: 'row', padding: 10, backgroundColor:'white', marginHorizontal: 20, shadowOffset:{width:0, height:0}, shadowColor:'black', shadowOpacity:0.2, elevation:1, marginTop: Platform.OS=='android'? 30: null}}>
                    <Icon name='ios-search' size={20}/>
                    <TextInput underlineColorAndroid="transparent" placeholder='Try Lagos Zone' placeholderTextColor="grey" style={{flex:1, fontWeight:'700', backgroundColor:'white'}}/>
                  </View>
                </View>
                <ScrollView scrollEventThrottle={16} showVerticalScrollIndicator={false}>
                  <View style={{flex:1, backgroundColor:'white', paddingTop:20}}>
                    <Text style={{fontSize:24, fontWeight:'700', paddingHorizontal:20}}>What can we help you find, Angy?</Text>
                    <View style={{height:130, marginTop:20}}>
                      <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                        <Category imageUri={require('../../assets/Picture1.png')} name="Brothers"/>
                        <Category imageUri={require('../../assets/fam.jpg')} name="Me"/>
                        <Category imageUri={require('../../assets/Picture1.png')} name="Us"/>
                        
                      </ScrollView>
                    </View>
                    <View style={{marginTop:40, paddingHorizontal:20}}>
                      <Text style={{fontSize:24, fontWeight:'700'}}>Introducing Airbnb Plus</Text>
                      <Text style={{fontWeight:'100', marginTop:10}}>A new selection of home verified for quality & comfort</Text>
                      <View style={{width:width-40, height:200, marginTop:20}}>
                        <Image style={{flex:1, height:null, width:null, resizeMode:'cover', borderRadius:5, borderWidth:1, borderColor:'#dddddd'}} source={require('../../assets/fam.jpg')}/>
                      </View>
                    </View>
                  </View>
                  <View style={{marginTop:40,}}>
                    <Text style={{fontSize:24, fontWeight: '700', paddingHorizontal: 20}}>Homes around the world</Text>
                    <View style={{paddingHorizontal:20, marginTop:20, flexDirection:'row', flexWrap: 'wrap', justifyContent: 'space-between'}}>
                      <Home imageUri={require('../../assets/fam.jpg')} width={width}/>
                      <Home imageUri={require('../../assets/fam.jpg')} width={width}/>
                      <Home imageUri={require('../../assets/fam.jpg')} width={width}/>
                      <Home imageUri={require('../../assets/fam.jpg')} width={width}/>
                      <Home imageUri={require('../../assets/fam.jpg')} width={width}/>
                    </View>
                  </View>
                </ScrollView>
              </View>
         </SafeAreaView>
  );
     }
  
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});