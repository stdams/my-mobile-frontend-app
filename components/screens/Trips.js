import React, { useState } from 'react';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, TextInput, FlatList } from 'react-native';
import Note from '../Trips/Note'
import Header from '../Trips/header'


export default function Main1() {

    const[notes, setNotes] = useState([
      {text: 'buy coffee', key: '1'},
      {text: 'create an app', key: '2'},
      {text: 'play on the switch', key: '3'}
    ])
    const [textInput, setTextInput] = useState('')

   const handleTextChange = (val) => {
     setTextInput(val)
   }
   
    const addNote = (textInput) => {
      
      setNotes((prevNotes)=>{
        return [ 
          {text:textInput, key: Math.random().toString()},
          ...prevNotes
        ]
      })
      setTextInput('')
    }
     

    const deleteNote = (keyval) => {
      setNotes((prevNotes) => {
        return prevNotes.filter(nope => nope.key != keyval)
      })
    }
  



  return(
    <View style={styles.container}>
      <View>
        <Header/>
      </View>
        <FlatList
        data={notes}
        renderItem={({item})=> (
          <Note note={item.text} keyval={item.key} deleteMethod={deleteNote}/>
        )}
        style={styles.scrollContainer} />
      <View style={styles.footer}>
        <TextInput style={styles.textInput} placeholder='>note' value={textInput} onChangeText={handleTextChange} placeholderTextColor='grey' underlineColorAndroid='transparent' />
      </View>
      <TouchableOpacity style={styles.addButton} onPress={()=>addNote(textInput)}>
        <Text style={styles.addButtonText}>+</Text>
      </TouchableOpacity>
    </View>
  )



}
    
 



const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  scrollContainer: {
    flex:1,
    marginBottom: 100
  },
  footer: {
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: 10
  },
  textInput: {
    alignSelf: 'stretch',
    color: '#fff',
    padding: 20,
    backgroundColor: '#252525',
    borderTopWidth: 2,
    borderTopColor: '#ededed'
  },
  addButton : {
    position: 'absolute',
    zIndex: 11,
    right: 20,
    bottom: 90,
    backgroundColor: '#e91e63',
    width: 90,
    height: 90,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 8
  },
  addButtonText: {
    color: '#fff',
    fontSize: 24
  }

});