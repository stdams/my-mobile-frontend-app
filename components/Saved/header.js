import React from 'react';
import { StyleSheet, Text, View, } from 'react-native';

 export default function Header() {
   return (
      <View style={styles.container}>
          <Text style={styles.title}>My Todos</Text>
      </View>
    );
  }



const styles = StyleSheet.create({
  container: {
    backgroundColor: 'coral',
    height: 80,
    paddingTop: 38,
  },
  title: {
      textAlign: 'center',
      color: '#fff',
      fontSize: 20,
      fontWeight: '700'
  }

});