import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';

export default function Header({note, key, deleteMethod}) {
    return(
        <View style={styles.header} key={key}>
            <Text style={styles.headerText}>NOTER</Text>
        </View>
    )
}

const styles = StyleSheet.create({
  header: {
      backgroundColor: '#e91e63',
      alignItems: 'center',
      justifyContent: 'center',
      borderBottomWidth: 10,
      borderBottomColor: '#ddd'
  },
  headerText: {
      color: 'white',
      fontSize: 18,
      padding: 26
  }
  
});